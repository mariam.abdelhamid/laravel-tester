@extends('layouts.app')

@section('content')



<div >
<a href="{{ url('/welcome') }}" style="margin-left:10%;">back</a>
</div>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading" style="margin-left:46%;"> Welcome </div>
        <div class="panel-body" style="margin-left: 25%; background-color:pink; padding: 10%; width: 50%;">
            
            <tr>
                <td> <p> Role: <strong>{{ Auth::user()->role }}</strong> </p></td>
                <td><p> name: <strong>{{ Auth::user()->name }}</strong></p> </td>
                <td><p> Email: <strong>{{ Auth::user()->email }}</strong></p> </td>
            </tr>
        
        
        </div>
    </div>
</div>

@endsection