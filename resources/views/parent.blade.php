<html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1"/>
    <title> Tester Project </title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
    <script>src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

</head>

<body>
    <div class="container">
        <br/>
        <h3 align="center" > Tester Project CRUD </h3>
        <br />
        @yield('main')
    </div>
</body>

</html>