@extends('parent')

@section('main')
@if($errors->any())

<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li> {{ $error }} </li>
        @endforeach

    </ul>
</div>
@endif

<div align="right">
    <a href="{{ route('crud.index')}}"> Back </a>
</div>


<form method="post" action="{{ route('crud.store')}}" enctype="multipart/form-data">
    @csrf
    <div>
    <label> Enter First Name </label>
        <div>
            <input type="text" name="first_name" />
        </div>
    </div>

    <br/>
    <br/>

    <div>
    <label> Enter Last Name </label>
        <div>
            <input type="text" name="last_name" />
        </div>
    </div>

    <br/>
    <br/>
    
    <div>
        <input type="file" name="image" />  
    </div>

    <br/>
    <div>
        <input type="submit" name="add" />  
    </div>

</form>
    
@endsection