<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('crud', 'CrudsController');

Route::get('/create','CreateController@index')->name('create');
Route::get('/welcome','HomeController@welcome')->name('welcome');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'HomeController@profile')->name('profile');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*Route::group([‘middleware’ => ‘App\Http\Middleware\AdminMiddleware’], function()
{
Route::match([‘get’, ‘post’], ‘/adminOnlyPage/’, "HomeController@admin");
});

Route::group([‘middleware’ => ‘App\Http\Middleware\StudentMiddleware’], function()
{
Route::match([‘get’, ‘post’], ‘/studentOnlyPage/’, "HomeController@member");
});*/
