<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crud;

class CrudsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $table = 'testing';
    public function index()
    {
        $data= Crud::latest()->paginate(5);
        return view('index', compact('data'))
        -> with ('i', (request()->input('page',1)-1) * 5);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validate
        $request->validate ( [
            'first_name'  => 'required',
            'last_name'   => 'required',
            'image'       => 'required|image|max:2048'

        ]);
        
        $image = $request->file('image');
        $new_name = rand() . '.'. $image->getClientOriginalExtension();
        $image->move(public_path('images'), $new_name);
        $form_data = array(
            'first_name'  => $request->first_name,
            'last_name'   => $request->last_name,
            'image'       => $new_name

        );
        Crud::create($form_data);
        /*Crud::create ([
                'first_name' => $form_data['first_name'],
                'last_name' => $form_data['last_name'],
                'image' => $form_data['image']
            ]);
        */
        return redirect('crud')->with('success', 'Data Added successfully.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
